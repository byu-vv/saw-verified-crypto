# SHA256 Cryptol Code

### About this code
This directory holds the Cryptol code that implements the SHA256 algorithm following the NIST FIPS PUB 180-4 specification. The `sha256.cry` file holds our "one-shot" functional implementation of SHA256. This code most closely resembles the NIST spec, and direct examination yields reasonable belief of equivalence between the spec and the code. The `sha256_iterative.cry` file holds our "iterative" implementation of SHA256. This code more closely resembles the approach OpenSSL takes to implementing SHA256, and it is this Cryptol code that we compare against our `reference.c` code in the other directories.  We have also included a copy of Galois SHA256 implementation in `galois_sha256.cry`  so that we can show proven equivalence between our implementation and theirs.  The only change we made to this code is the module name so that it matches the file name we selected.

### Test vectors
The NIST spec defines specific test vectors that can be used to provide adequate assurance of an implementation's correctness. As an added measure of showing equivalence between the Cryptol code and the NIST spec, we have set up both our implementations to run on those NIST test vectors. The `tv` directory holds the code required to perform those test vector runs. The `sha256tv.cry` file contains all the test vectors in Cryptol code and runs them against both Cryptol implementations.

### Cryptol properties
Cryptol has a built-in feature know as a property that we use to prove equivalence of two different Cryptol codes. Here are our properties:
```
property sha256_iter_equiv msg = SHA256 msg == sha256_iterative msg
property sha256_galois_equiv msg = SHA256 msg == gal::SHA256 msg
```
They state that for an arbitrary input, `msg`, the outputs of `SHA256` and `sha256_iterative` and `gal::SHA256` (Galois implementation) are equivalent. We use SAW to prove this property for a boundary set of input sizes (SAW passes the equivalence proof to an SMT solver).

### Run SAWScript command
You can use the command below to run the test vectors and equivalence proofs from the tv directory. `sha_tvs.saw` is a simple SAWscript that will print out the results of each test vector function, and also run the property on our previously selected sizes.
```
saw sha_tvs.saw
```
This command assumes that the program `saw` is on the path environment variable.  It also assumes that the directory `cryptol` is on the CRYPTOLPATH enviromnent variable.  Instructions for adding to CRYPTOLPATH can be found in `sha_properties.cry`.