This repository holds C, Cryptol and SAWScript code used for verification of OpenSSL's SHA256 implementation using the [Software Analysis Workbench](https://saw.galois.com/) (SAW) created by [Galois, Inc](https://galois.com/).

We chose to work on this because the paper by Chudnov et al (see references) proves the s2n HMAC implementation correct with the assumption that the underlining SHA256 C implementation is correct. Amazon's s2n does not implement SHA256. Instead it allows the developer to choose between several implementations, one of which is OpenSSL's implementation.

# Programming Languages and Tools
We use three programming languages: C, [Cryptol](https://cryptol.net/) and [SAWScript](https://github.com/GaloisInc/saw-script). We specifically us [LLVM](https://llvm.org/) as our C compiler, because it is required by SAW. We also use three SAT/SMT solvers, which how built-in support in SAW: [Z3](https://github.com/Z3Prover/z3), [ABC](https://people.eecs.berkeley.edu/~alanmi/abc/), and [Yices](https://yices.csl.sri.com/).

### C with LLVM
All C code is compiled with LLVM to the LLVM intermediate representation referred to as LLVM bitcode.

### Cryptol
Cryptol is a functional programming language used to specify cryptographic implementations. The language facilitates easy implementation of cryptographic algorithms with syntax that matches their mathematical specifications (such as NIST FIPS publications). We concur with the claim that visual inspection of Cryptol implementations for correctness is straightforward and reliable, opposed to visual inspection of C code implementations.

### SAWScript
SAWScript is the language used to interact with SAW. See the [manual](https://github.com/GaloisInc/saw-script/blob/master/doc/manual/manual.md) for more details on SAWScript.

# Software Analysis Workbench (SAW)
We use SAW to prove functional equivalence of C code to Cryptol code. SAW creates function models of each code using [symbolic execution](https://en.wikipedia.org/wiki/Symbolic_execution). The functional models are stored as SAWCore terms, know as *Terms*. A Term has a value that is able to precisely describe all possible computations performed by the program from which it was created. SAW loads C compiled LLVM bitcode modules and then symbolically executes the bitcode to create a Term. Cryptol is natively compiled into a Term. SAW proves that two Terms are equivalent by passing off the proof to SMT solvers. SAW has built-in support for Z3, ABC, and Yices.

### Limitations
SAW can only construct functional models from C programs with fixed-size inputs, outputs, and loops. The ramifications are that we can only prove equivalence of cryptographic codes for a specific message size. Thus, we must re-run our proofs to prove multiple message sizes. We've used boundary value analysis to choose specific message lengths for our proofs. When feasible, we've run exhaustive proofs for the full range of all possible values. We've also found that large loops result in path explosion, which leads to large Term sizes that do not complete in the backend SMT solvers. SAW has some built-in features that allow us to reduce Term size. By reducing Term size, we are able to overcome path explosion. We have also found that modifying memory in a variable way leads large Terms. To handle this, we have to decompose functions to separate out memory modification.

### Overrides
All C functions are symbolically executed at invocation by SAW, effectively inlining function calls to create a "flat" term.

SAW has a composition framework for proofs that allows us to prove functions separately, and then *override* function calls.
When an override is applied during symbolic execution, the following steps occur:

1. The function call of the override is not followed
2. All preconditions of the override are verified
3. All postconditions of the override are applied to the state of the symbolic execution

This is done by using the built-in feature in SAW of applying method specifications to a proof. By first proving a method specification correct, we can then use it later to override the computation that would have been required in the function call. This allow us to decompose a proof of code into smaller proofs of functions. We can then override function calls in a full proof of the code. Using this incremental approach, we have resolved both the issue of path explosion on loops and variable memory modifications.

### General Use
SAW can be used to prove other properties of the functional models it creates. We don't use it, but SAW has support for creating functional models of Java code (from symbolic execution of JVM bytecode).

### Trusted Code Base (TCB)
The TCB includes the LLVM bitcode compiler, SAW's conversion from LLVM bitcode to SAWCore terms, the Cryptol compiler to SAWCore terms, SAW's interface with SMT solvers, and standard lib function replacements that work with SAW’s symbolic execution.

### Downloading SAW and Solvers
We recommend that you use the binaries provided by Galois to download SAW.  You can find these binaries on the [SAW tool website](https://saw.galois.com/downloads.html).  Links to each of the solvers we used can also be found on this site.

# Repository Structure
We have a folder for each OpenSSL SHA256 function that we prove using SAW. Currently, we have proofs for:

1. SHA256_Init
2. sha256_block_data_order
3. SHA256_Final

Besides the `SHA256_Init` function, we had to make modifications to the functions to prove them equivalent to Cryptol. To do this, we created C Reference files with function derived from OpenSSL that contain the needed modifications. We also have an automated proof tha the OpenSSL `sha256_block_data_order` function is equivalent to our C Ref `sha256_block` function. We do not currently have such a proof for the two `SHA256_Final` implementations, but they are trivially verified using visual inspection. We are working on an automated proof.

### Run SAWScript command
Because the Cryptol code is in a different directory from each SAWScript proof, you will need to add the Cryptol directory to your `CRYPTOLPATH` enviroment variable. You can do this from the command line with the command below (replace "user_name" and "..." with your own username and any extra pathing needed to get to the `saw-verified-crypto` directory).
```
export CRYPTOLPATH="/home/user_name/.../saw-verified-crypto/cryptol:$CRYPTOLPATH" 
```

# Resources and References
Chudnov A. et al. (2018) Continuous Formal Verification of Amazon s2n. In: Chockler H., Weissenbacher G. (eds) Computer Aided Verification. CAV 2018. Lecture Notes in Computer Science, vol 10982. Springer, Cham. https://doi.org/10.1007/978-3-319-96142-2_26

Dockins R., Foltzer A., Hendrix J., Huffman B., McNamee D., Tomb A. (2016) Constructing Semantic Models of Programs with the Software Analysis Workbench. In: Blazy S., Chechik M. (eds) Verified Software. Theories, Tools, and Experiments. VSTTE 2016. Lecture Notes in Computer Science, vol 9971. Springer, Cham. https://doi.org/10.1007/978-3-319-48869-1_5

Erk ok, L., Matthews, J.: Pragmatic equivalence and safety checking in Cryptol, p.73. ACM Press (2008)


