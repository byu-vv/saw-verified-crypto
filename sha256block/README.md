# SHA256 Block Function

Neither the OpenSSL nor the C Reference complete verification when compared to the Cryptol for equivalence, *but*
when the two Terms extracted from the C codes are compared for equivalence, the proof completes successfully. We
then use the compositional approach to override function calls in our C Reference to prove equivalence to the Cryptol.

### Term sizes for and Proof result for equivalence with Cryptol
Implementation | loc | Proof result 
---------------|-----|-------------
OpenSSL sha256_block_data_order | 35815 | Timeout
C Reference sha256_block | 35739  | Timeout
Cryptol hash_block_iterative | 398 | N/A

### Overrides and Average runtime Proof results for `compress`
Overrides | Z3 | ABC | Yices 
---------------|-------------|-------------|-------------
None | Timeout | Timeout | Timeout 
`compute_T1` and `compute_T2` | 1 second | Timeout | 0.54 seconds

### Overrides and Average runtime Proof results for `prepare`
Overrides | Z3 | ABC | Yices 
---------------|-------------|-------------|-------------
None | Timeout | Timeout | Timeout 
`compute_W` | 0.3 seconds | 0.27 seconds | 0.28 seconds

### Overrides and Average runtime Proof results for `sha256_block`
Overrides | Z3 | ABC | Yices 
---------------|-------------|-------------|-------------
None | Timeout | Timeout | Timeout 
`prepare` and `compress` | 0.45 seconds | Timeout | 0.46 seconds


### C code compile commands
##### OpenSSL implementation bitcode file
```
clang -g -emit-llvm -o openssl.bc -c openssl.c
```
##### C Reference implementation bitcode file
```
clang -g -emit-llvm -o reference.bc -c reference.c
```
##### Stress Test implementation bitcode file
```
clang -g -emit-llvm -o stresstest.bc -c stresstest.c
```

### Command to run the proof
```
saw block.saw
```
