# Looping Computation Explosion

### About this example
The code for this example comes from the `sha256block` folder in this repository.  As shown in the example, the following lines of code cause issues when we loop any significant number of times. The `compute_XXX` functions need to be overridden to complete the proof.

```
for (i = 0; i <= LOOPS; i++) {
    T1 = compute_T1(e, f, g, h, K256[i], W[i]);
    T2 = compute_T2(a, b, c);
    ...
}
```
where
```
SHA_LONG compute_T1(SHA_LONG e, SHA_LONG f, SHA_LONG g, SHA_LONG h,
                         SHA_LONG k, SHA_LONG w) {
    return h + Sigma1(e) + Ch(e, f, g) + k + w;
}

SHA_LONG compute_T2(SHA_LONG a, SHA_LONG b, SHA_LONG c) {
    return Sigma0(a) + Maj(a, b, c);
}
```

In this stress test, we drop down the loop count to see where the reasonable limit is of running the computation loops.  The table below shows the proof runtimes of this function without using overrides of the computations on various loop counts.  As can quickly be seen, the increasing loop count causes a drastic increase in the runtime ... to the point that it quickly becomes an unviable proof.  These times are a stark contrast from the times required to run the proof using an override of the computation function as shown in `sha256block`.

### Runtime averages
We know from our `sha256block` that the ABC SMT solver will not complete on this function even with overrides in place, so for the purposes of these runtime averages, we didn't include ABC.  The loop count in this case is zero based to be consistent with how the code operates given a specific value for the `LOOPS` variables.  We count the proof as non-terminating when the proof time exceeds 15 minutes.

|LOOP|Z3 (seconds)|Yices (seconds)|
|:---:|:---|:---|
|0|0.0499779|0.0478021|
|1|1.9814446|1.9138773|
|2|3.1220186|3.0936714|
|3|7.542717|7.4715957|
|4|66.3466761|65.9884752|
|5|350.5182208|339.9296005|
|6|Timeout|Timeout|


The loop count can be altered to try different numbers by changing the two `LOOPS` constants in the Cryptol and C code.  Be sure you recompile the C code after changing the constant before running the SAWScript.

Cryptol:
```
type LOOPS = 0
```
C:
```
# define LOOPS 0
```

# Looping with Overrides

### About this example
As shown in that example, We have overridden the `compute_XXX` functions to show how much easier the proof completes when the looping doesn't have to symbolically execute the computation over and over for each iteration.

```
T1 = compute_T1(e, f, g, h, K256[i], W[i]);
T2 = compute_T2(a, b, c);
```

As can be seen from the averages below, using this verification technique of creating function overrides inside of loops allows the solvers to deal with significantly larger loop counts with relative ease.

The only modification we made that differs from the `sha256block` code is that we had to modify the `K256` and `W` array access in the `compute_T1` arguments, so that they don't index out-of-bounds. We added a modulo 64 (`%64`) for each so that we would stay in bounds.This allows us to run this compress function at any desired loop count.

As with the looping explosion example, we will not be using ABC, because it failed to complete a proof on the compress function even with all the overrides in place. The loop count in this table is also zero based to be consistent with how the code operates given a specific value for the LOOPS variables. We count it as no longer completing when the proof time exceeds 15 minutes.

|LOOPS |Z3 (seconds)|Yices (seconds)|
|:---:|:---|:---|
|0 | 0.035604 | 0.0327805|
|5 | 0.0588952 | 0.0533447|
|63 | 1.181702 | 0.5425783|
|100 | 2.4586186 | 0.9788704|
|200 | 9.2570838 | 2.5113631|
|400 | 33.0437336 |	6.8116108|
|800 | 148.805978 |	21.4133141|
|1600 |	658.3078032 | 74.3078413|
|3200 |	Timeout | 283.8617083|
|6400 | N/A |Timeout|

The loop count can be altered to try different numbers by changing the two `LOOPS` constants in the Cryptol and C code. Be sure you recompile the C code after changing the loop count before running the SAWScript. Note: because of how large the loop count can get, the print to console can get unwieldy. The tool will output a message each time an override is used, which means that for a loop count of 800 the tool will print out over 1600 lines of output. This passes the typical console history line count, so if you want access to every line, make sure you push the output to a file. Also, because of the significant output generated with larger loop counts, it was easier logistically to only run one solver proof at a time. By going to the SAWScript, you can comment out the proof-running lines for the solver you aren't using, or have them both uncommented if you want to run them in the same execution. The proof-running lines are grouped together by solver at the end of the SAWScript.
Loop count code:

Cryptol:
```
type LOOPS = 63
```
C:
```
# define LOOPS 63
```


# Running the Examples

### C code compile command
```
clang -g -emit-llvm -o stresstest.bc -c stresstest.c
```

### Run SAW command to execute SAWScript
```
saw stresstest.saw
```
This command assumes that the program `saw` is on the path environment variable.