/*
 * Copyright 1999-2018 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the Apache License 2.0 (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */

#include <string.h>

# define SHA256_DIGEST_LENGTH    32
# define SHA_LBLOCK 16
# define SHA_CBLOCK (SHA_LBLOCK*4)
# define SHA_LONG unsigned int

typedef struct SHA256state_st {
    SHA_LONG h[8];
    SHA_LONG Nl, Nh;
    //SHA_LONG data[SHA_LBLOCK];    // <-- Original OpenSSL
    // Because of endianness we changed `data` to be a character array.
    //   The issue is that OpenSSL only used `data` between `SHA256_Update` and
    //   `SHA256_Final` in a controlled way, but it doesn't account for endianess,
    //   which causes issues when we do a proof of `SHA256_Final` standone to our
    //   Cryptol, which is big endian. Just changing to a byte array fixes the issue
    //   and preserves the meaning of the code.
    unsigned char data[SHA_CBLOCK]; // <-- Slight update to OpenSSL
    unsigned int num, md_len;
} SHA256_CTX;

int SHA256_Init(SHA256_CTX *c)
{
    memset(c, 0, sizeof(*c));
    c->h[0] = 0x6a09e667UL;
    c->h[1] = 0xbb67ae85UL;
    c->h[2] = 0x3c6ef372UL;
    c->h[3] = 0xa54ff53aUL;
    c->h[4] = 0x510e527fUL;
    c->h[5] = 0x9b05688cUL;
    c->h[6] = 0x1f83d9abUL;
    c->h[7] = 0x5be0cd19UL;
    c->md_len = SHA256_DIGEST_LENGTH;
    return 1;
}