# SHA256 Init Function

### About this function
This function is used by OpenSSL to initialize the memory of their context struct when beginning a SHA256 hash.
This shows that fixed-sized memory modification code that is easy to verify with SAW. As other functions show, modifying memory in a variable sized way causes issues.

Note that because of the limitations of SAW in converting between LLVM and Cryptol types, we had to manually create a struct in the SAWScript using the initialized values from the Cryptol function to compare against our C struct.

This proof completes with all three SMT solvers (see table below with average runtimes).

### Term sizes
Here are the Term size comparisons in their corresponding Term lines of code:
|C function `SHA256_Init`|Cryptol function `sha256_init`|
|:---:|:---:|
|34|23|

This is an excellent example of how Cryptol code is natively supported in SAW. It takes 16 lines in the Term extracted from the C code to set sixteen 32-bit words to zero (each line of the Term just sets a 32-bit word to zero). The Term compiled from the Cryptol code is only three lines, as shown below:
```
Cryptol.ecZero (Prelude.Vec 64 x@2)
  (Cryptol.PZeroSeq (Cryptol.TCNum 64) x@2
     (Cryptol.PZeroSeqBool (Cryptol.TCNum 8))),
```

### Runtime averages 
z3: 0.10783508 seconds  
abc: 0.11032876 seconds  
yices: 0.09260594 seconds

### Command to run the proof
```
saw init.saw
```

### Command to print the Terms
```
saw init_printterms.saw
```

### C code compile command
```
clang -g -emit-llvm -o init.bc -c init.c
```