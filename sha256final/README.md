# SHA256_Final Function

### About this function
The `SHA256_Final` function is used by OpenSSL to perform the final manipulations for the message digest (this involves appending certain data bytes to the block and performing one or two final hashes).

This function shows some of the issues when trying to use SAW to verify legacy code. Symbolic execution of this function fails. We created a separate C Reference function that is derived from the OpenSSL code. Our reference code replaces the `OPENSSL_Cleanse` and first two `memset` function calls which do not work with the symbolic execution. Our reference code also shows specific lines of code that must be decomposed into smaller functions. This decomposition is needed to reduce the size and complexity of the term created from symbolic execution.

Note that because of the limitations of SAW in converting between LLVM and Cryptol types, we had to manually create a struct in the SAW script using the initialized values from the cryptol function to compare against our C struct.

### Limitations
This code snip shows the parts of the OpenSSL function that cause issues:
```
int HASH_FINAL(unsigned char *md, HASH_CTX *c)
{
    unsigned char *p = (unsigned char *)c->data;
    size_t n = c->num;

    p[n] = 0x80;                
    n++;

    if (n > (HASH_CBLOCK - 8)) {
        memset(p + n, 0, HASH_CBLOCK - n);
        HASH_BLOCK_DATA_ORDER(c, p, 1);
        ...
    }
    memset(p + n, 0, HASH_CBLOCK - 8 - n);

    ...

    return 1;
}
```
##### Memset
As mentioned above, the calls to `memset` cannot be modeled in the SAWScript proofs because it takes `p + n` as a parameter. Both `p` and `n` are modeled as symbolic variables, and there is no way to create a symbolic variable to point to `p` at offset `n` (note the symbolic variables are really refering to the hash context `c` where `n` is `c->num` and `p` is `c->data`). This limitation is easily overcome by creating a new function to perform the needed computation (we note that the `memset` is used to set all values in the array past the offset `n` to 0). We created the function `zero_fill`. This function has to be written in a way so that the loop initializer and bounds are constants. Also, by creating this function to match the Cryptol code, it is trivially handled by SAW in our proof.

##### Memory modifications with symbolic variables
There is another place in the function, `SHA256_Final`, where `n` is used to modify the memory pointed to by `p`:
```
   p[n] = 0x80;                
   n++;
```
Again, if `n` is a symbolic variable then the created term has to model all possible computations of modifying `p`. This creates a massive term (see table with term sizes). We fix this by wrapping these two lines of code into a function -- `add_one_bit`, prove the equivalence to the corresponding Cryptol code, and then use an override in our SAWScript. This decomposition allows SAW to prove just one piece at a time.

### Overriding existing functions
SAW is intended for compositional proofs (since it passes off the final stage of proofs to SMT solvers). The OpenSSL function `SHA256_Final` calls one function, `sha256_block_data_order`. We have a separate folder in this repo (`sha256Block`) that discusses our proof for `sha256_block_data_order`. Our reference code uses the function `sha256_block` in our implementation of `SHA256_Final`, because this function most be decomposed to complete verification.

### C Reference
Here is the code for our modified `SHA256_Final` function that shows the replacements for the two limitations:
```
int SHA256_Final(unsigned char *md, SHA256_CTX *c)
{
    ...
    
    add_one_bit(c); // this replaces the lines "p[n] = 0x80; n++;"

    if (c->num > (SHA256_CBLOCK - 8)) {
        zero_fill(c->data, c->num, SHA256_CBLOCK);
        sha256_block(c->h, c->data);
        ...
    }
    zero_fill(c->data, c->num, SHA256_CBLOCK - 8);
    
    ...

    return 1;
}
```

### Term sizes and Proof results with no overrides
C Reference (term loc) | Cryptol (term loc) | Proof result 
---------------|------|---------------
zero_fill (328) | zero_fill (39) | Success
add_one_bit (6050) | add_one_bit (26) | Success
sha256_block (35501) | hash_block_iterative (398) | Timeout
SHA256_Final (41940) | sha_finalize (1499) | Timeout

Success is when we prove the C code equivalent to the Cryptol code.

NOTE: see the folder `sha256block` for our decomposition and proof for `sha256_block` used as an override below.

See how SHA256_Final can be verified when the right functions are overridden.

Override setup 1:
* `sha256_block`

Override setup 2:
* `sha256_block` and `zero_fill`

Override setup 3:
* `sha256_block` and `add_one_bit`

Override setup 4:
* `sha256_block`, `zero_fill`, and `add_one_bit`

### Overrides and Average runtime Proof results for SHA256_Final
Overrides | Z3 | ABC | Yices 
---------------|-------------|-------------|-------------
`sha256_block` | Timeout | Timeout | Timeout
`sha256_block` and `zero_fill` | Timeout | Timeout | Timeout
`sha256_block` and `add_one_bit` | Timeout | Timeout | 5.1 seconds
`sha256_block`, `zero_fill`, and `add_one_bit` | Timeout | Timeout | 5.9 seconds

As can be seen, neither Z3 nor ABC were able to complete, which confirms our belief that Yices is the best solver for what we are trying to do.  As we continue to complete the proof, Yices is the solver we plan to use.

### SAW commands
##### Run final.saw proof script
```
saw final.saw
```
##### Run final_printterms.saw proof script and print to a file
```
saw final_printterms.saw > terms.txt
```

### C code compile commands
##### OpenSSL implementation bitcode file
```
clang -g -emit-llvm -o openssl.bc -c openssl.c
```
##### C Reference implementation bitcode file
```
clang -g -emit-llvm -o reference.bc -c reference.c
```