/*
 * This code is derived from the OpenSSL Project. See copyright, and license below.
 */
/*
 * Copyright 1999-2018 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the Apache License 2.0 (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define DATA_ORDER_IS_BIG_ENDIAN

#define ROTATE(a,n)     (((a)<<(n))|(((a)&0xffffffff)>>(32-(n))))

#if defined(DATA_ORDER_IS_BIG_ENDIAN)

# define HOST_c2l(c,l)  (l =(((unsigned long)(*((c)++)))<<24),          \
                         l|=(((unsigned long)(*((c)++)))<<16),          \
                         l|=(((unsigned long)(*((c)++)))<< 8),          \
                         l|=(((unsigned long)(*((c)++)))    )           )
# define HOST_l2c(l,c)  (*((c)++)=(unsigned char)(((l)>>24)&0xff),      \
                         *((c)++)=(unsigned char)(((l)>>16)&0xff),      \
                         *((c)++)=(unsigned char)(((l)>> 8)&0xff),      \
                         *((c)++)=(unsigned char)(((l)    )&0xff),      \
                         l)

#elif defined(DATA_ORDER_IS_LITTLE_ENDIAN)

# define HOST_c2l(c,l)  (l =(((unsigned long)(*((c)++)))    ),          \
                         l|=(((unsigned long)(*((c)++)))<< 8),          \
                         l|=(((unsigned long)(*((c)++)))<<16),          \
                         l|=(((unsigned long)(*((c)++)))<<24)           )
# define HOST_l2c(l,c)  (*((c)++)=(unsigned char)(((l)    )&0xff),      \
                         *((c)++)=(unsigned char)(((l)>> 8)&0xff),      \
                         *((c)++)=(unsigned char)(((l)>>16)&0xff),      \
                         *((c)++)=(unsigned char)(((l)>>24)&0xff),      \
                         l)

#endif

# define SHA_LONG unsigned int
# define SHA_LONG_64BIT unsigned long long

# define SHA_LBLOCK      16
# define SHA_CBLOCK      (SHA_LBLOCK*4)/* SHA treats input data as a
                                        * contiguous array of 32 bit wide
                                        * big-endian values. */
# define SHA_LAST_BLOCK  (SHA_CBLOCK-8)
# define SHA_DIGEST_LENGTH 20


# define SHA256_CBLOCK   (SHA_LBLOCK*4)/* SHA-256 treats input data as a
                                        * contiguous array of 32 bit wide
                                        * big-endian values. */

typedef struct SHA256state_st {
    SHA_LONG h[8];
    SHA_LONG Nl, Nh;
    unsigned char data[SHA_CBLOCK];
    unsigned int num, md_len;
} SHA256_CTX;

# define SHA256_DIGEST_LENGTH    32

static const SHA_LONG K256[64] = {
    0x428a2f98UL, 0x71374491UL, 0xb5c0fbcfUL, 0xe9b5dba5UL,
    0x3956c25bUL, 0x59f111f1UL, 0x923f82a4UL, 0xab1c5ed5UL,
    0xd807aa98UL, 0x12835b01UL, 0x243185beUL, 0x550c7dc3UL,
    0x72be5d74UL, 0x80deb1feUL, 0x9bdc06a7UL, 0xc19bf174UL,
    0xe49b69c1UL, 0xefbe4786UL, 0x0fc19dc6UL, 0x240ca1ccUL,
    0x2de92c6fUL, 0x4a7484aaUL, 0x5cb0a9dcUL, 0x76f988daUL,
    0x983e5152UL, 0xa831c66dUL, 0xb00327c8UL, 0xbf597fc7UL,
    0xc6e00bf3UL, 0xd5a79147UL, 0x06ca6351UL, 0x14292967UL,
    0x27b70a85UL, 0x2e1b2138UL, 0x4d2c6dfcUL, 0x53380d13UL,
    0x650a7354UL, 0x766a0abbUL, 0x81c2c92eUL, 0x92722c85UL,
    0xa2bfe8a1UL, 0xa81a664bUL, 0xc24b8b70UL, 0xc76c51a3UL,
    0xd192e819UL, 0xd6990624UL, 0xf40e3585UL, 0x106aa070UL,
    0x19a4c116UL, 0x1e376c08UL, 0x2748774cUL, 0x34b0bcb5UL,
    0x391c0cb3UL, 0x4ed8aa4aUL, 0x5b9cca4fUL, 0x682e6ff3UL,
    0x748f82eeUL, 0x78a5636fUL, 0x84c87814UL, 0x8cc70208UL,
    0x90befffaUL, 0xa4506cebUL, 0xbef9a3f7UL, 0xc67178f2UL
};

/*
 * Note that FIPS180-2 discusses "Truncation of the Hash Function Output."
 * default: case below covers for it. It's not clear however if it's
 * permitted to truncate to amount of bytes not divisible by 4. I bet not,
 * but if it is, then default: case shall be extended. For reference.
 * Idea behind separate cases for pre-defined lengths is to let the
 * compiler decide if it's appropriate to unroll small loops.
 */
#define HASH_MAKE_STRING(c,s)   do {    \
        unsigned long ll;               \
        unsigned int  nn;               \
        switch ((c)->md_len)            \
        {                \
            case SHA256_DIGEST_LENGTH:  \
                for (nn=0;nn<SHA256_DIGEST_LENGTH/4;nn++)       \
                {   ll=(c)->h[nn]; (void)HOST_l2c(ll,(s));   }  \
                break;                  \
            default:                    \
                if ((c)->md_len > SHA256_DIGEST_LENGTH) \
                    return 0;                           \
                for (nn=0;nn<(c)->md_len/4;nn++)                \
                {   ll=(c)->h[nn]; (void)HOST_l2c(ll,(s));   }  \
                break;                  \
        }                               \
        } while (0)

/*
 * FIPS specification refers to right rotations, while our ROTATE macro
 * is left one. This is why you might notice that rotation coefficients
 * differ from those observed in FIPS document by 32-N...
 */
# define Sigma0(x)       (ROTATE((x),30) ^ ROTATE((x),19) ^ ROTATE((x),10))
# define Sigma1(x)       (ROTATE((x),26) ^ ROTATE((x),21) ^ ROTATE((x),7))
# define sigma0(x)       (ROTATE((x),25) ^ ROTATE((x),14) ^ ((x)>>3))
# define sigma1(x)       (ROTATE((x),15) ^ ROTATE((x),13) ^ ((x)>>10))

# define Ch(x,y,z)       (((x) & (y)) ^ ((~(x)) & (z)))
# define Maj(x,y,z)      (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))

static SHA_LONG compute_W(SHA_LONG a, SHA_LONG b, SHA_LONG c, SHA_LONG d) {
    return sigma1(a) + b + sigma0(c) + d;
}

static void prepare(SHA_LONG* W, const unsigned char* data) {
    int i;
    SHA_LONG l;

    for (i = 0; i < 16; i++) {
        (void)HOST_c2l(data, l);
        W[i] = l;
    }

    for (; i < 64; i++) {
        W[i] = compute_W(W[i-2], W[i-7], W[i-15], W[i-16]);
    }
    
}

static SHA_LONG compute_T1(SHA_LONG e, SHA_LONG f, SHA_LONG g, SHA_LONG h,
                         SHA_LONG k, SHA_LONG w) {
    return h + Sigma1(e) + Ch(e, f, g) + k + w;
}

static SHA_LONG compute_T2(SHA_LONG a, SHA_LONG b, SHA_LONG c) {
    return Sigma0(a) + Maj(a, b, c);
}

static void compress(SHA_LONG* H, SHA_LONG* W) {
    unsigned int a, b, c, d, e, f, g, h, T1, T2;
    int i;

    a = H[0];
    b = H[1];
    c = H[2];
    d = H[3];
    e = H[4];
    f = H[5];
    g = H[6];
    h = H[7];

    for (i = 0; i < 64; i++) {
        T1 = compute_T1(e, f, g, h, K256[i], W[i]);
        T2 = compute_T2(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;
    }

    H[0] += a;
    H[1] += b;
    H[2] += c;
    H[3] += d;
    H[4] += e;
    H[5] += f;
    H[6] += g;
    H[7] += h;
}

static void sha256_block(SHA256_CTX *c, const void* in)
{
    const unsigned char* data = in;
    SHA_LONG W[64];

    prepare(W, data);
    compress(c->h, W);
}

int SHA256_Final(unsigned char *md, SHA256_CTX *c);

// This function does not complete symbolic execution in SAW, because the
//   loop initializer and bound are both dependent on symbolic variables
void zero_fill_unbounded(unsigned char* p,
                         unsigned int start,
                         unsigned int end)
{
    unsigned int i;
    for (i = start; i < end; i++) {
        p[i] = 0;
    }
}

// These functions work with symbolic execution in SAW
unsigned char zero_data(unsigned char value,
                        unsigned int i,
                        unsigned int start,
                        unsigned int end)
{
    unsigned char result = 0;
    if (i < start || i >= end) {
        result = value;
    }
    return result;
}

void zero_fill(unsigned char* p,
               unsigned int start,
               unsigned int end)
{
    unsigned int i;
    for (i = 0; i < SHA256_CBLOCK; i++) {
        p[i] = zero_data(p[i], i, start, end);
    }
}

void add_one_bit(SHA256_CTX *c)
{
    c->data[c->num] = 0x80;
    c->num++;
}

int SHA256_Final(unsigned char *md,
                 SHA256_CTX *c)
{
    unsigned char *p = (unsigned char *)c->data;

    add_one_bit(c);

    if (c->num > (SHA256_CBLOCK - 8)) {
        zero_fill(p, c->num, SHA256_CBLOCK);
        c->num = 0;
        sha256_block(c, p);
    }
    zero_fill(p, c->num, SHA256_CBLOCK - 8);
    
    p += SHA256_CBLOCK - 8;
#if   defined(DATA_ORDER_IS_BIG_ENDIAN)
    (void)HOST_l2c(c->Nh, p);
    (void)HOST_l2c(c->Nl, p);
#elif defined(DATA_ORDER_IS_LITTLE_ENDIAN)
    (void)HOST_l2c(c->Nl, p);
    (void)HOST_l2c(c->Nh, p);
#endif
    p -= SHA256_CBLOCK;

    sha256_block(c, p);
    c->num = 0;
    memset(p, 0, SHA256_CBLOCK);

    HASH_MAKE_STRING(c, md);
    return 1;
}