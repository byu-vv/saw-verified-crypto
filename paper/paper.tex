% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{listings, color}
\usepackage{graphicx}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
%\renewcommand\UrlFont{\color{blue}\rmfamily}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{almond}{rgb}{0.94, 0.87, 0.8}
\definecolor{amaranth}{rgb}{0.9, 0.17, 0.31}
\definecolor{azure}{rgb}{0.0, 0.5, 1.0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}

\lstdefinestyle{customc}{
  frame=none,
  language=C,
  basicstyle=\footnotesize\ttfamily, 
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  numbers=left,
  keywordstyle=\bfseries\color{azure},
  commentstyle=\itshape\color{dkgreen},
  identifierstyle=\color{black},
  stringstyle=\color{almond},
  numbersep=-1pt,
  numberstyle=\color{gray},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3,
}

\begin{document}
%
\title{Towards verifying SHA256 in OpenSSL with the Software Analysis Workbench}
%
\titlerunning{Towards verifying SHA256 in OpenSSL in SAW}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
\author{Brett Decker \and
Benjamin Winters \and
Eric Mercer\orcidID{0000-0002-2264-2958}}
%
\authorrunning{Decker, Winters, Mercer}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{Brigham Young University, Provo, UT 84601, USA\\
\email{\{bdecker,egm\}@cs.byu.edu}\\}
%
\maketitle              % typeset the header of the contribution
%
\begin{abstract}
The Software Analysis Workbench (SAW) is a verification tool that has shown promise in verifying cryptographic implementations in C and Java as evidenced by the verification of Amazon’s s2n HMAC. That result uses an idealized abstraction for SHA256 to simplify the proof obligations. The OpenSSL SHA256 implementation supported by s2n presents verification problems for SAW because some functions do not complete SAW's symbolic execution or verification in the backend Satisfiability Modulo Theories (SMT) solvers. Fortunately, SAW provides a compositional framework to reduce the proof complexity, replacing function calls with overrides which are nothing more than contracts stating the input to output relation of the functions. This paper illustrates the SAW compositional framework applied to the OpenSSL SHA256 implementation. It shows a refactoring of the implementation that manages symbolic variables to work with the symbolic execution engine and lends itself to overrides while preserving its connection to the original source. Early results suggest the approach effective in applying SAW to legacy cryptographic implementations that are critical to the security of almost all existing applications.

\keywords{Cryptography \and Verification \and SAW \and Symbolic execution \and Cryptol \and SHA \and Compositional proof.}
\end{abstract}
%
%
%
\section{Introduction}
Cryptographic algorithms are the basis for secure software systems. As these systems become larger and more complex, the need to verify security becomes ever more important. Traditional methods of verification, such as code inspections, pose difficulties that have been well documented \cite{bacchelli}. Formal verification reduces the need for code inspections but also increases domain expertise. In this paper, we describe our use of SAW to prove correctness of OpenSSL SHA256 \cite{galois}.

SAW's primary use is to prove functional equivalence of cryptographic algorithms in C or Java to Cryptol \cite{cryptol}. Cryptol is a domain-specific language with syntax matching cryptographic mathematical specifications, \textit{e.g.} NIST FIPS publications. SAW creates functional models from C, Java, and Cryptol codes represented as \textit{terms}, which have a value that precisely describes all possible computations of the code. Terms are created using their intermediate representations and symbolic execution for C and Java code \cite{saw}. Cryptol code is compiled into a term directly. SAW's native support for Cryptol allows the term to be expressed succinctly. SAW passes a proof of term equivalence to SMT solvers, with built-in support for Z3, ABC, and Yices \cite{saw}. It is important to note that SAW verification results rely on two underlying assumptions: 1) that the Cryptol code is correct and 2) that LLVM and JVM preserve the meaning of the original code.

Legacy cryptographic code, such as OpenSSL's SHA256, often cannot be verified in SAW as-is. Highly-optimised code must be refactored into smaller functions. Loop bounds must be made concrete, limiting the generality of the proof. SAW's compositional framework allows proofs to be built from the bottom up, replacing function calls in upper-level functions with overrides. An override is a contract stating the input to output relation for the function call. This allows a series of smaller proofs to be given to SMT solvers. We present our work in refactoring the OpenSSL SHA256 implementation into a C Reference implementation that completes verification in SAW using its compositional framework. This represents domain knowledge that is not readily apparent in the SAW documentation.

\subsubsection{Related Work}
\cite{chudnov} uses SAW to prove Amazon's s2n \cite{s2n} HMAC implementation correct (incorporating work from \cite{beringer}). The s2n HMAC proof assumes the underlying SHA256 C implementation is correct. s2n does not implement SHA256, instead supporting existing legacy implementations, such as OpenSSL.

\section{Compositional Proofs for OpenSSL SHA256}
As with all tools, SAW has limitations \cite{saw}. SAW can only construct terms for a subset of C and Java programs that have fixed-size inputs, outputs, and loop iterations. One ramification for cryptographic codes is that we can only prove equivalence for a specific message size (\textit{e.g.} a proof for SHA256 with message size of 128 bytes). Thus, we must re-run the proofs for different sizes message or construct an auxiliary proof to generalize the results to arbitrary message sizes.

The issues of applying SAW to the OpenSSL SHA256 implementation can be summarized as follows:
\begin{enumerate}
    \item Symbolic execution errors when memory reads and writes are not statically guaranteed to be inside the fixed-size allocations and when there are volatile function calls.
    
    \item Symbolic execution fails to complete when memory allocations are not concrete (fixed-size) and loop initializers and bounds are not fixed-sized, and both are complicated by function calls that use symbolic values.

    \item The backend SMT solvers fail to complete when symbolic execution results in a large or dissimilar -- from Cryptol -- term.
    
    %All functions are symbolically executed at invocation by SAW, effectively inlining function calls to create a ``flat" term \cite{saw}.
\end{enumerate}
We chose our timeout for issues 2) and 3) to be 15 minutes -- anything longer is not practical vis-a-vis development.

All three of the above issues are illustrated in the proofs for OpenSSL functions \texttt{SHA256\_Final} and \texttt{sha256\_block\_data\_order} (see \cite{repo} for proofs scripts and code). The compositional proofs require refactoring the OpenSSL code. The refactored code suitable for SAW is referred to as the C Reference implementation throughout the rest of this paper (see Figure \ref{fig:sha_final} for a side-by-side comparison of the OpenSSL and C Reference codes).

\subsubsection{Symbolic Execution Errors}
The function \texttt{SHA256\_Final} as implemented by OpenSSL errors on symbolic execution. First, symbolic execution errors because \texttt{OPENSSL\_Cleanse} at line 278 of Figure \ref{fig:sha_final}(a) calls \texttt{memset} that is annotated as a volatile function to prevent the call from being reordered (or removed) by the compiler. We replace the call to a static-sized, non-volatile \texttt{memset} call. Removing the volatile key word potentially changes the behavior of the compiler, allowing it to move the call to \texttt{memset}, but verifying the LLVM compiler to bitcode is outside the scope of SAW and this work. Second, symbolic execution errors because the memory of \texttt{p} is written at offset \texttt{n}, with \texttt{n} being a symbolic value (line 261). In such a case, SAW cannot know if the offset is inside the allocation for \texttt{p}. We add a precondition in our proof script to constrain \texttt{n} to be less than the allocated size of \texttt{p}. This simple constraint enables the symbolic execution to not error out and start building a term for the function.

\input{sha_final_figure}

\subsubsection{Symbolic Execution Fails to Complete}
\texttt{SHA256\_Final} in the C Reference fails to complete symbolic execution within the allotted time bound and requires further refactoring. The calls to \texttt{memset} at lines 265 and 269 are problematic because they again use \texttt{n}, a symbolic variable, to determine the loop bound. We cannot override these function calls because the first parameter, \texttt{p + n}, cannot be modeled in SAW -- there is no way in SAW to create a memory offset by a symbolic variable, as is required. Thus, we must modify the code and create a new function, \texttt{zero\_fill}, to replace \texttt{memset} at lines 265 and 269. The \texttt{zero\_fill} function must have a constant loop initialization and bound. This leads to a functional style implementation, opposed to how one would normally implement such a function in C (Figure \ref{fig:zero_fill} compares the two). Here \texttt{zero\_fill} is implemented in a functional style to match its later Cryptol specification, below, since it will require an override to not timeout in the SMT solver as shown in the next section.

\input{zero_fill_cryptol}

\input{zero_fill_cs}

Manual inspection easily verifies equivalence of \texttt{memset} and \texttt{zero\_fill}. With these modifications, the C Reference \texttt{SHA256\_Final} completes symbolic execution but fails to complete in the backend SMT solvers for our specified bound.

\subsubsection{SMT Solvers Fail to Complete}
We already identified a potential issue at line 261 of \texttt{SHA256\_Final} where the symbolic variable \texttt{n} is used as an offset to memory. Since Cryptol cannot be implemented in like manner, the resulting terms will be very dissimilar (as shown in Table \ref{final1table}, row 2). We encapsulate lines 261-262 into a function, \texttt{add\_one\_bit}, to facilitate a smaller proof which we then use as an override. \texttt{SHA256\_Final} has multiple other function calls that contribute to the large term size, since functions are symbolically executed at invocation by SAW, effectively inlining them to create a ``flat" term \cite{saw}. We create overrides for \texttt{zero\_fill} and \texttt{sha256\_block\_data\_order} to use in addition to the override for \texttt{add\_one\_bit}. The overrides enable SAW to prove the C Reference equivalent to its Cryptol specification. The new overrides create additional proof obligations as the overrides must now be shown equivalent to the C code they replace. For example, SAW must prove that \texttt{sha256\_block\_data\_order} is equivalent to its Cryptol override, only that proof does not complete in the backend SMT solver; thus, we must first refactor the C function to finish this secondary proof obligation.

\input{hash_block_figure}

The C Reference renames \texttt{sha256\_block\_data\_order} to \texttt{sha256\_block}. We must reduce the term size for the function, but there are no function calls in the body since all bit manipulations are encapsulated as C macros. In refactoring the function we want to make it as similar to the Cryptol as possible as that is likely to reduce the complexity of the equivalence proof. This entails rewriting the OpenSSL speed optimization for the block hash portion with the unoptimized, simple NIST specification (see comparisons in Figure \ref{fig:compress}). We also modify \texttt{sha256\_block} to perform a single block, thus decreasing term size by removing the outer loop. We also had to break apart \texttt{sha256\_block} into two functions -- the split is at line 205 in Figure \ref{final2table}(b). As such, the first two loops and the last loop are encapsulated into function calls. In each of these two new functions, the most complex computations inside loops \texttt{W[i]}, \texttt{T1}, and \texttt{T2} (lines 202-203 and 207-209) must also be refactored into functions. See our repository \cite{repo} for full details of the refactor.

We prove \texttt{sha256\_block\_data\_order} and \texttt{sha256\_block}  equivalent to preserve the result of equivalence from OpenSSL to Cryptol. This proof completes in less than three seconds even though the term sizes are each over 35,000 loc. This illustrates the power of the SAW feature to compare C codes (with pointer arguments) for equivalence. SAW makes heavy use of term rewriting \cite{saw}, and, as shown by our proof results, the effectiveness of the rewriting is enhanced when comparing two codes from the same programming language.

With overrides for \texttt{add\_one\_bit}, \texttt{zero\_fill}, and \texttt{sha256\_block\_data\_order}, the proof for \texttt{SHA256\_Final} completes and succeeds. Table \ref{final1table} shows the extracted term size of the overridden functions as well as the result of proving each function equivalent to its Cryptol implementation. Table \ref{final2table} shows which overrides affect the end result of completing the proof. It is noteworthy that only Yices completes the proof. In our experience in proving SHA256, Yices handles loops with large bounds better than Z3 or ABC. Also, note that the override for \texttt{zero\_fill} does not impact proof completion because it is implemented in a functional style that causes the term to be trivially proved equivalent to the Cryptol. Thus SAW's rewriting engine is more efficient in reducing the \texttt{zero\_fill} terms without an override.

\begin{table}[t]
\caption{Term sizes and Proof results without overrides}\label{final1table}
\setlength{\tabcolsep}{6.5pt}
\begin{tabular}{|l|l|l|l|l|}
\hline
\textbf{C Reference} & \textbf{Term loc} & \textbf{Cryptol} & \textbf{Term loc} & \textbf{Proof result} \\
\hline
zero\_fill & 328 & zero\_fill & 39 & Success \\
append\_one\_bit & 6050 & append\_one\_bit & 26 & Success \\
sha256\_block & 35501 & hash\_block\_iterative & 398 & Timeout \\
SHA256\_Final & 41940 & sha\_finalize & 1499 & Timeout \\
\hline
\end{tabular}
\end{table}

\begin{table}[b]
\caption{Average runtime proof results for SHA256\_Final}\label{final2table}
\setlength{\tabcolsep}{10.5pt}
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{Overrides} & \textbf{Z3} & \textbf{ABC} & \textbf{Yices} \\
\hline
sha256\_block & Timeout & Timeout & Timeout \\
sha256\_block, zero\_fill & Timeout & Timeout & Timeout\\
sha256\_block, add\_one\_bit & Timeout & Timeout & $\sim$ 5 secs \\
sha256\_block, zero\_fill, add\_one\_bit & Timeout & Timeout & $\sim$ 6 secs \\
\hline
\end{tabular}
\end{table}

\section{Results and Conclusion}
This paper shows that SAW is able to prove correctness of cryptographic algorithms implemented in C and Java to Cryptol specifications if the code is refactored to have 1) statically fixed-size memory accesses and non-volatile functions, 2) fixed-sized memory allocations and loop iterations, and 3) small functions encapsulating computationally complex code.

We show how to use SAW to prove the correctness of OpenSSL functions \texttt{SHA256\_Final} and \texttt{sha256\_block\_data\_order} leveraging SAW's compositional framework. We have proved the C Reference is equivalent to the Cryptol code which we have also proved is equivalent to the Cryptol implementation use in the s2n HMAC proof \cite{s2n} (see \cite{repo}). Using the methods described in this paper, we are working to prove the entire OpenSSL SHA256 implementation with SAW. We are working to fully automate the proof of equivalence between the OpenSSL and C Reference codes, instead of partially relying on visual inspection. Finally, we plan to apply this work to verify other SHA256 implementations supported by s2n.

%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
\bibliographystyle{splncs04}
\bibliography{paper}

\end{document}
